<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.main');
})->name('main');
Route::get('/register-attorney', 'AuthController@getRegistrationAttorney')->name('registration-attorney');
Route::get('/register-physician', 'AuthController@getRegistrationPhysician')->name('registration-physician');
Route::post('/register-attorney', 'AuthController@postRegistrationAttorney');
Route::post('/register-physician', 'AuthController@postRegistrationPhysician');
Route::get('/login', 'AuthController@getLogin')->name('login');
Route::post('/login', 'AuthController@postLogin');
Route::get('/test', 'AuthController@test');