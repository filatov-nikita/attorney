<style>
    label {
        display: block;
    }
</style>
<div class="container">
    <h2>Авторизация</h2>
    <form action="" method="POST">
        {{csrf_field()}}
        <div><label for="">Email</label><input type="text" name="email"></div>
        <div><label for="">Password</label><input type="password" name="password"></div>
        <div><label for="">Remember me?</label><input type="checkbox" name="remember"></div>
        <div><input type="submit" value="Login"></div>
    </form>
    @if(session('authError'))
        <div>{{session('authError')}}</div>
    @endif
</div>