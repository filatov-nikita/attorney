<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->belongsTo('App\Models\Role');
    }

    /**
     * The relation personal_info_attorney
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne::class
     */
    public function personal_info_attorney()
    {
        return $this->hasOne('App\Models\Personal_info_attorney');
    }
    /**
     * The relation personal_info_physician
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne::class
     */
    public function personal_info_physician()
    {
        return $this->hasOne('App\Models\Personal_info_physician');
    }
}
