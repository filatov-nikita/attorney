<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personal_info_attorney extends Model
{
    protected $fillable = ['user_id'];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
