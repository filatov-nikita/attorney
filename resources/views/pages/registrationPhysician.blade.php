<style>
    label {
        display: block;
    }
    .error {
        color:red;
    }
</style>
<div class="container">
    <h2>Регистрация</h2>
    <form action="" method="POST">
        {{csrf_field()}}
        <div>
            <label for="">first_name</label><input type="text" name="first_name" value="{{old('first_name')}}">
             @if ($errors->has('first_name'))
                <div class="error">{{$errors->first('first_name')}}</div>
             @endif
        </div>
        <div>
            <label for="">last_name</label><input type="text" name="last_name" value="{{old('last_name')}}">
             @if ($errors->has('last_name'))
                 <div class="error">{{$errors->first('last_name')}}</div>
             @endif
        </div>
        <div>
            <label for="">email</label><input type="text" name="email" value="{{old('email')}}">
             @if ($errors->has('email'))
                <div class="error">{{$errors->first('email')}}</div>
             @endif
        </div>
        <div>
            <label for="">password</label><input type="password" name="password">
             @if ($errors->has('password'))
                <div class="error">{{$errors->first('password')}}</div>
            @endif
        </div>
        <div>
            <label for="">confirmed password</label><input type="password" name ="password_confirmation">
            @if ($errors->has('password_confirmation'))
                <div class="error">{{$errors->first('password_confirmation')}}</div>
            @endif
        </div>
        <div><input type="submit" value="register"></div>
    </form>
</div>