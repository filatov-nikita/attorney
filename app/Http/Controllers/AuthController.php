<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\RequestRegistrationAttorney;
use App\Http\Requests\RequestRegistrationPhysician;
use App\Models\User;
use App\Models\Role;
use App\Models\Personal_info_attorney;
use App\Models\Personal_info_physician;

class AuthController extends Controller
{
    public function getRegistrationAttorney() 
    {
        return view('layouts.primary', ['page' => 'pages.registrationAttorney']);
    }

    public function test() 
    {
        
    }
    
    public function postRegistrationAttorney(RequestRegistrationAttorney $request) 
    {
        $role = Role::where('name', 'attorney')->first();
        $user = User::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role_id' => $role->id
        ]);
        $user->personal_info_attorney()->create([]);
    }
    
    public function getRegistrationPhysician() 
    {
        return view('layouts.primary', ['page' => 'pages.registrationPhysician']);
    }
    
    public function postRegistrationPhysician(RequestRegistrationPhysician $request) 
    {
        $role = Role::where('name', 'physician')->first();
        $user = User::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role_id' => $role->id
        ]);
        $user->personal_info_physician()->create([]);
    }

    public function getLogin() 
    {
        return view('layouts.primary', ['page' => 'pages.login']);
    }

    public function postLogin(Request $request) 
    {
        $authResult = Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ], $request->input('remember'));
        
        if ($authResult) { return redirect()->route('main'); } 
        else {     
            return redirect()
            ->route('login')
            ->with('authError', 'Неправильный логин или пароль!');   
        }
    }
}
