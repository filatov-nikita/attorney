<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestRegistrationPhysician extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Messages for errors requests
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'email.unique'  => 'Email is already exists',
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*' => 'required',
            'first_name' => 'min:3|max:55',
            'last_name' => 'min:3|max:55',
            'email' => 'email|unique:users',
            'password' => 'confirmed'
        ];
    }
}
